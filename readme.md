# Description 

This project provides a test Access database for
[AccessMake](https://gitlab.com/juengling/AccesMake-Test) or other purposes. It shall ensure that
every test scenario starts with the same situation.

The Access database is part of this repository because of the test character of this project. Usually I would **not** add any .accdb/.accde or the older .mdb/.mde files. This would be redundant and -- from the source code control point of view -- a really bad idea: Access databases are big binary files, not suitable for working with source code control in a helpful way.

This is why tools like [OASIS](https://dev2dev.de/) or [Ivercy](http://www.ivercy.com/) exist. Use one of them instead of committing your Access database in one big chunk.

# What do you need?

- [This repository](https://gitlab.com/juengling/AccesMake-Test)
- [Microsoft Access](https://products.office.com/de-de/access) (any of the current versions is supposed to work)
- [Git](https://git-scm.com/) or [TortoiseGit](https://tortoisegit.org/) (current version)
- [OASIS](https://dev2dev.de/) (current version)


# How to get started

1. Move to a folder where you store your projects
1. Clone this repository with Git `git clone https://gitlab.com/juengling/AccesMake-Test`
1. Edit `test.cmd` and check the path to your Access installation and version, change if necessary
1. Start `test.accdb` to verify it works
1. Run `test.cmd` in your command window (this will delete `test.accdb` and recreate it from scratch)  
1. Start `test.accdb` again to verify it still works

Whenever you start the Access database it should display this message:

![Test message](test-message.png "Test message")

Look at the code to get the details, it's open source :-)

# Data

The test data in the tables came from [Memory Alpha](http://de.memory-alpha.wikia.com/). History is never complete, but it may give you a feeling of future possibilities.

# Collaborate

Feel free to fork this repository and provide any enhancements by a pull request. Or use the [issue tracker](https://gitlab.com/juengling/AccesMake-Test/issues) to make recommendations.