There are many ways to contribute to this project.

- Fork this repository and provide any enhancements by a pull request.
- Use the [issue tracker](https://gitlab.com/juengling/AccesMake-Test/issues) to file bug reports or recommendations.
